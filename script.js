let buttonenviar = document.querySelector(".button");
let buttonInfo = document.querySelector(".button2");
let msgcard = document.querySelector("#card");
let bd = "https://treinamentoajax.herokuapp.com/messages";

//GET
const fetchGET = () =>{
    fetch(bd) 
    .then(response => response.json())
    .then(response => {
       let responseordenada = ordenaporID(response);
        msgcard.innerHTML = '';
        responseordenada.forEach(manipulaDOM);
    })
}

buttonInfo.addEventListener("click", fetchGET);


//POST
const fetchPOST = () =>{
    let nome = document.querySelector(".esp1").value;
    let msg = document.querySelector(".esp2").value;
    let fetchBody = {
        "message":{
        "name": nome,
        "message": msg
        }
    }

    let fetchConfig = {
        method: "POST",
        headers: {"Content-Type":"application/JSON"},
        body: JSON.stringify(fetchBody)
    }

    fetch(bd, fetchConfig)
    .then(response => response.json())
    .then(response => {
        manipulaDOM(response);
        document.querySelector(".esp1").value = "";
        document.querySelector(".esp2").value = "";
    })
}

buttonenviar.addEventListener("click", fetchPOST)

//GET POR ID
const fetchGetID = () => {
    let id = document.querySelector(".id-input").value;
    fetch(bd+'/'+id)
    .then(response => response.json())
    .then(response => {
        msgcard.innerHTML = '';
        manipulaDOM(response)
    })
}

let buttonID = document.querySelector(".button3");
buttonID.addEventListener("click", fetchGetID)


//DELETE
const fetchDelete = (id) => {
    fetch(bd+'/'+id, {method: "DELETE"})
}
//


//EDIT
const fetchPut = (id) => {
    let nomeeditado = document.querySelector(".editn").value
    let msgeditada = document.querySelector(".editm").value
    let fetchBody = {
        "message":{
        "name": nomeeditado,
        "message": msgeditada
        }
    }
    let fetchConfig = {
        method: "PUT",
        headers: {"Content-Type":"application/JSON"},
        body: JSON.stringify(fetchBody)
    }
    fetch(bd+'/'+id, fetchConfig)
    .then(response => response.json())
    .then(response =>{
        let editado = document.getElementById('${id}')
        let p_da_lista = editado.querySelectorAll("p")
        let nome_p = p_da_lista[0]
        let msg_p = p_da_lista[1]
        console.log(nome_p)
        nome_p.innerHTML = nomeeditado
        msg_p.innerHTML = msgeditada
    })
}


const toggleEdit = (id) => {
    let div = document.getElementById('${id}')
    let form = div.querySelector(".form-edit")
    form.style.display = form.style.display=='flex'? 'none' : 'flex'
}


//GERAL
const ordenaporID = (array) => {
    let msgordenada = array.sort((a, b) => {
        if (a.id > b.id){
            return 1;
        }else{
            return -1;
        }
    })
    return msgordenada;
}

const manipulaDOM = (item) => {
    let elemento = document.createElement('div');
    elemento.innerHTML = '<h1> '+item.name+' </h1> <p>' +item.message+ '</p><button onclick="fetchDelete('+item.id+')" class ="button4">Delete</button><div class = "form-edit"><input class="editn" placeholder ="Edite seu nome"/><input class="editm" placeholder ="Edite sua mensagem"/><button id = "confirma-edit" onclick = "fetchPut('+item.id+')">Confirmar</button><button id = "cancela-edit" onclick = "toggleEdit('+item.id+')">Cancelar</button></div>'
    elemento.classList.add("stylecard");
    msgcard.appendChild(elemento);
}
 
